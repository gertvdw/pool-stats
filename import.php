<?php

include('lib/configuration.php');
include('lib/database.php');
$database = new Database($dbsettings);

// disable output
ob_start();

if (isset($_POST['submit']) && $_POST['submit'] == 'send') {
    $info = $_POST['info'];
    if (strlen($info) == 0) {
        ob_end_clean();
        header("Location: index.php");
        exit();
    }


    $lines = explode("\n", $info);
    $game = array();
    $set = false;
    $game['test'] = strtotime('apr 19, 2013 15:34');
    $game['raw'] = $info;
    foreach ($lines as $l) {
        if (preg_match("/^[a-z]{2}\. ([a-z]+)\.? ([0-9]+\, [0-9]+ [0-9]{2}\:[0-9]{2})/", $l, $ml)) {
            $m = (isset($monthMap[$ml[1]])) ? $monthMap[$ml[1]] : $ml[1];
            $game['str'] = sprintf("%s %s", $m, $ml[2]);
            $game['ts'] = date("Y-m-d H:i:00", strtotime($m . ' ' . $ml[2]));
            continue;
        }
        if (preg_match("/^([a-zA-Z]+) vs ([a-zA-Z]+)/", $l, $ml)) {
            $game['player_a'] = $database->getPlayerId($ml[1]);
            $game['player_b'] = $database->getPlayerId($ml[2]);
            continue;
        }
        if (preg_match("/^Score/", $l)) {
            $set = 'score';
            continue;
        }
        if (preg_match("/^Average/", $l)) {
            $set = 'avg';
            continue;
        }
        if (preg_match("/^High Run/", $l)) {
            $set = 'high';
            continue;
        }
        if (preg_match("/^Innings/", $l)) {
          $set = 'innings';
          continue;
        }
        if ($set !== false && $set != 'innings') {      
            $l = preg_replace("/[\(\)]/", "", $l);
            $items = explode(" ", $l);

            $game[$set . '_a'] = $items[0];
            for ($i=1; $i<count($items); $i++) {
                if (doubleval($items[$i]) > 0.000) {
                    $game[$set . '_b'] = $items[$i];
                    $set = false;
                    continue 2;
                }
            }
        }
        if ($set == 'innings') {
          // innings are put consecutive
          if (!preg_match("/[0-9]/", $l)) {
              // empty line, so stop capturing inning info
              $set = false;
              continue;
          }

          // strip parentheses, we know whose points are where
          $l = preg_replace("/[\(\)]/", "", $l);
          $items = array_filter(explode(' ', $l), 'strlen');
          $game['innings'][] = $items;
        }
    }
    //echo "<pre>" . print_r($game, true) . "</pre>";
    $database->createGame($game);
}

ob_end_clean();
header("Location: index.php");