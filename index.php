<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pool stats</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <style type='text/css'>
            #topheader {
              background: #474747;
              color: #D2D2D2;
              height: 2em;
              /*
              left: 0;
              position: absolute;
              right: 0;
              top: 0;
              */
              font-size: 2em;
            }
            #header {
                /*
              position: absolute;
              top: 3em;
              left: 0;
              right: 0;
                background: none repeat scroll 0 0 #F1F1F1;
              
              height: 3.5em;
              */
              
            }
            
            #header ul.sbtn {
                list-style-type: none;
            }
            #header ul.sbtn li {
                display: block;
                float: left;
            }
            #header ul.sbtn a {
                color: #2D2D2D;
                display: block;
                height: 25px;
                text-decoration: none;
            }
            #header ul.sbtn li:first-child {
                border-radius: 4px 0 0 4px;
                border-width: 1px;
            }
            #header ul.sbtn li:last-child {
                border-radius: 0 4px 4px 0;
                border-width: 1px 1px 1px 0;
            }
            #header ul.sbtn li {
                background: none repeat scroll 0 0 #ECECEC;
                border-color: #C6C6C6;
                border-style: solid;
                border-width: 1px 1px 1px 0;
                cursor: pointer;
                line-height: 25px;
                padding: 0;
                text-align: center;
                width: 85px;
            }
            #header ul.sbtn li:hover, #header ul.sbtn li.selected {
                background: none repeat scroll 0 0 #E5E5E5;
            }
            #header .sbtn {
                float: left;
            }
            body {
                padding-top: 60px;
            }
        </style>
        <link href="css/bootstrap-responsive.css" rel="stylesheet">        
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
              <div class="container">
                <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="brand" href="#">14.1</a>
                <div class="nav-collapse collapse">
                  <ul class="nav">
                    <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="#uploadForm" data-toggle='modal'>Upload game</a></li>
                    <li><a href="#contact">Contact</a></li>
                  </ul>
                </div><!--/.nav-collapse -->
                
              </div>
            </div>
        </div>
        <div id='header'>
            <ul class='sbtn'>
                <li class='selected'>
                    <a href='#' class='setChart' data-chart='score' data-title='Scores'>Scores</a>
                </li>
                <li>
                    <a href='#' class='setChart' data-chart='avg' data-title='Game averages'>Average</a>
                </li>
                <li>
                    <a href='#' class='setChart' data-chart='high' data-title='High runs'>High</a>
                </li>
            </ul>
        </div>
        <div class='container-fluid'> 
            <div class='row-fluid'>
                <div class='span12'>
                    <div id='container' style='width: 100%; height: 300px'></div>
                </div>
            </div>
            
        </div>
        
        <!-- upload form -->
        <div id='uploadForm' class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Upload a new game</h3>
            </div>
            <div class="modal-body">

                    <form method='post' action='import.php'>
                        <fieldset>
                            <label>
                                <textarea name='info' placeholder='Paste game stats here'></textarea>
                            </label>
                            <input type='hidden' name='submit' value='send'/>
                            <button type='submit' class='btn btn-primary'>Send</button>
                        </fieldset>
                    </form>
                
            </div>
            <div class="modal-footer">
                
            </div>
        </div>

        <script type='text/javascript' src='js/jquery-1.9.1.min.js'></script>
        <script type='text/javascript' src='js/highcharts/highcharts.src.js'></script>
        <script type='text/javascript' src='js/highcharts/modules/exporting.src.js'></script>
        <script type='text/javascript' src='js/generate_graph.js'></script>
        <script type='text/javascript' src='js/poolstats.js'></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
