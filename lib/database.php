<?php


class Database
{
    private $connection = null;
    
    /**
     * Create a database connection
     * @param Array $settings See configuration.php
     */
    public function __construct($settings)
    {
        $cstring = sprintf("mysql:host=%s;dbname=%s",
                $settings['database_host'],
                $settings['database_db']);
        try {
            $this->connection = new PDO($cstring,
                    $settings['database_user'],
                    $settings['database_pass']);
        } catch (PDOException $e) {
            die("ERROR!" . $e->getMessage());
        }        
    }
    
    /**
     * Retrieve all game scores
     * @return Array
     */
    public function getGameScores()
    {
        $stmt = $this->connection->prepare(
                "SELECT g.*, a.name AS name_a, b.name AS name_b 
                FROM straight_game g 
                JOIN (SELECT id, name FROM player) a ON a.id = g.player_a 
                JOIN (SELECT id, name FROM player) b ON b.id = g.player_b
                ORDER BY ts ASC");
        $stmt->execute();
        $data = array();
        while ($row = $stmt->fetch()) {
            if (preg_match("/^\d{4}\-([0-9]+\-[0-9]+)/", $row['ts'], $ml)) {
                $row['simpledate'] = $ml[1];
            }
            $data[] = $row;
        }
        
        return $data;
    }
    
    /**
     * Create a new game, save the raw input and inning info
     * @param Array $parameters
     */
    public function createGame($parameters)
    {
        $stmt = $this->connection->prepare(
                "INSERT INTO straight_game 
                 (player_a, player_b, ts, score_a, score_b, avg_a, avg_b, high_a, high_b)
                    VALUES
                    (:playera, :playerb, :ts, :scorea, :scoreb, :avg_a, :avg_b, :high_a, :high_b)");
        $stmt->bindParam(":playera", $parameters['player_a']);
        $stmt->bindParam(":playerb", $parameters['player_b']);
        $stmt->bindParam(":ts", $parameters['ts']);
        $stmt->bindParam(":scorea", $parameters['score_a']);
        $stmt->bindParam(":scoreb", $parameters['score_b']);
        $stmt->bindParam(":avg_a", $parameters['avg_a']);
        $stmt->bindParam(":avg_b", $parameters['avg_b']);
        $stmt->bindParam(":high_a", $parameters['high_a']);
        $stmt->bindParam(":high_b", $parameters['high_b']);
        $stmt->execute();
        //echo "Create game: " . $stmt->errorCode() . "|" . $stmt->errorInfo() . "<br/>";
        
        $game_id = $this->connection->lastInsertId();
        
        $stmt = $this->connection->prepare("INSERT INTO straight_raw (game_id, raw) VALUES (:game_id, :raw)");
        $stmt->bindParam(":game_id", $game_id);
        $stmt->bindParam(":raw", $parameters['raw']);
        $stmt->execute();
        //echo "Save raw: " . $stmt->errorCode() . "|" . $stmt->errorInfo() . "<br/>";
        //echo "<pre>" . print_r($this->connection->errorInfo(), true) . "</pre>";

        foreach ($parameters['innings'] as $inning) {
            $this->addInning($game_id, $inning);
        }
    }

    /**
     * Record a single inning
     * @param int $game_id The ID for the game (from createGame())
     * @param Array $inning 0=>delta_a, 1=>score_a, 7=>delta_b, 8=>score_b
     */
    private function addInning($game_id, $inning) {
        $stmt = $this->connection->prepare("INSERT INTO straight_inning (game_id, score_a, score_b, delta_a, delta_b)
            VALUES (:game_id, :score_a, :score_b, :delta_a, :delta_b)");
        $stmt->bindParam(":game_id", $game_id);
        
        $keys = array_keys($inning);    // irregularly numbered due to removed spaces..

        $stmt->bindParam(":delta_a", $inning[$keys[0]]);
        $stmt->bindParam(":score_a", $inning[$keys[1]]);
        $stmt->bindParam(":delta_b", $inning[$keys[2]]);
        $stmt->bindParam(":score_b", $inning[$keys[3]]);

        $stmt->execute();
        //echo "Save inning: " . $stmt->errorCode() . "|" . $stmt->errorInfo() . "<br/>";
    }
    
    /**
     * Retrieve a players ID by playername
     * @param String $name
     * @return int
     */
    public function getPlayerId($name) {
        $stmt = $this->connection->prepare(
                "SELECT id FROM player WHERE name = :player");
        $stmt->bindParam(":player", $name);
        $stmt->execute();
        if ($row = $stmt->fetch()) {
            return $row['id'];
        }
        return false;
    }
}