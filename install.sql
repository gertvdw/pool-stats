SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `email` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `straight_game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_a` int(11) NOT NULL DEFAULT '0',
  `player_b` int(11) NOT NULL DEFAULT '0',
  `ts` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `score_a` int(11) NOT NULL DEFAULT '0',
  `score_b` int(11) NOT NULL DEFAULT '0',
  `avg_a` double NOT NULL,
  `avg_b` double NOT NULL,
  `high_a` int(11) NOT NULL,
  `high_b` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `straight_inning` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL DEFAULT '0',
  `score_a` int(11) NOT NULL DEFAULT '0',
  `score_b` int(11) NOT NULL DEFAULT '0',
  `delta_a` int(11) NOT NULL DEFAULT '0',
  `delta_b` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `game_id` (`game_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `straight_raw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL,
  `raw` text CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_gameid` (`game_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `straight_stats` (
  `id` int(11) NOT NULL DEFAULT '0',
  `game_id` int(11) NOT NULL DEFAULT '0',
  `player_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
