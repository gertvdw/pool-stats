<?php

require_once 'lib/configuration.php';
require_once 'lib/database.php';

$db = new Database($dbsettings);
$op = $_GET['op'];

if ($op == 'getGames') {
    $games = $db->getGameScores();
    echo json_encode($games);
}

exit(0); 