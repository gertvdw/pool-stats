// Show chart
$('.setChart').click(function(){
	render_type = $(this).data('chart');
	$('.sbtn li').each(function(){
		$(this).removeClass('selected');
	});
	$(this).parent().addClass('selected');
    my_chart.setTitle(null, { text: $(this).data('title')});
	update_graph();	// do 
        
});

/**
 * Set an onresize event to limit the number of points shown on small screens.
 */
$(window).resize(function() {
    process_resize();
});

/**
 * Fitted into separate function so we can call it during initial loading
 * of the graph.
 * @returns max number of items in graph
 */
function process_resize() {
    var itemCount = 0; 
    var w = $(window).width();
    if (w < 600)
        itemCount = 5;
    else if (w < 800)
        itemCount = 10;
    
    if (MAX_ITEMS != itemCount) {
        MAX_ITEMS = itemCount;
    
        update_graph();
    }
    return itemCount;
}// end