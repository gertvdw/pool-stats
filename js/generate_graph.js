"use strict";
// JSLint options:
/* global jQuery $ console Highcharts */

var my_chart; // global var for the created charts
var do_init = true; // set to false after first run
var MAX_ITEMS = -1; // assume a screen wide enough to show all games
var render_type = 'score'; // default, show the game scores
var COLOR_SCHEME = 'default';
var playerToSeriesMap = [];
playerToSeriesMap[1] = 0;
playerToSeriesMap[2] = 1;



var options = {       
        chart: {
            renderTo: 'container',
            type: 'areaspline'
        },
        title: {
            text: 'Game stats'
        },
        subtitle: {
            text: 'Scores'
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: '#FFFFFF'
        },
        xAxis: {
            categories: [
                
            ]
        },
        yAxis: {
            title: {
                text: 'Points'
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' pts'
        },
        //credits: {
            //enabled: false,
            
        //},
        
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        exporting: {
            sourceWidth: 800,  // todo: sourceWidth: $(window).width()
            // sourceHeight: 200,
            // scale: 2 (default)
            chartOptions: {
                subtitle: null
            }
        },
        
        series: [],
        loading: {
            showDuration: 100
        }
    };

$(function () {
    if (COLOR_SCHEME == 'grey')
        options.colors = ['#888888', '#CCCCCC'];

    my_chart = new Highcharts.Chart(options);
    process_resize();
});

function update_graph() {
    if (do_init !== true) {
        my_chart.series[0].setData([], true);
        my_chart.series[1].setData([], true);
    }
    my_chart.showLoading();
    $.ajax({
        url: 'ajax.php?op=getGames',
        dataType: 'json',
        success: function(json) {
            var o,x,series_id_a,series_id_b;

            // assume just 2 known players
            for (var i = 0; i < json.length; i++) {
                // limit the number of points shown
                if (MAX_ITEMS > 0 && json.length - i > MAX_ITEMS) {
                    console.log("proces " + json.length + " - " + i + " > " + MAX_ITEMS);
                    continue;
                }
                o = json[i];
                x = o.simpledate;
                series_id_a = playerToSeriesMap[o.player_a];
                series_id_b = playerToSeriesMap[o.player_b];

                if (do_init) {
                    do_init = false;
                    // set up the series names, player map, etc.
                    my_chart.addSeries({ name: o.name_a, data: [] });
                    my_chart.addSeries({ name: o.name_b, data: [] });
                }
                
                options.xAxis.categories.push(x);
                var val_a = 0;
                var val_b = 0;
                
                if (render_type == 'score') {
                    val_a = parseInt(o.score_a);
                    val_b = parseInt(o.score_b);
                    
                } else if (render_type == 'avg') {
                    val_a = parseFloat(o.avg_a);
                    val_b = parseFloat(o.avg_b);
                } else if (render_type == 'high') { 
                    val_a = parseInt(o.high_a);
                    val_b = parseInt(o.high_b);
                }
                my_chart.series[playerToSeriesMap[o.player_a]].addPoint(val_a, false);
                my_chart.series[playerToSeriesMap[o.player_b]].addPoint(val_b, false);
            }
            my_chart.redraw();
            my_chart.hideLoading();
            
        }
    });
}

